window.addEventListener('load', function() {
	//stran nalozena
	
	//Dodaj novo barvo
	var dodajBarvo = function(event) {
		var input = document.createElement('button');
        var picker = new jscolor(input);
        picker.fromRGB(Math.floor(Math.random()*255), Math.floor(Math.random()*255), Math.floor(Math.random()*255))
        document.getElementById("barve").appendChild(input);
	}
		
	//6.4.2
		document.querySelector("#novaBarva") 
		.addEventListener('click', dodajBarvo);
	//6.4.2 ends here
	
	//Odstrani barve
	
	//6.4.3
	var odstraniBarve = function(event) {
		var elementWithIdBarve = document.getElementById("barve");
		
		
		while (elementWithIdBarve.hasChildNodes()) {   
			elementWithIdBarve.removeChild(elementWithIdBarve.firstChild);
		}
	}
	//6.4.3 ends here
	
	
	document.querySelector("#odstraniBarve") 
		.addEventListener('click', odstraniBarve);
	
	//Stroboskop
	var vrednosti = [];
	//var minCas = 0;
	//var maxCas = 0;
	//6.4.4
	var minCas = document.getElementById("min").value;
	var maxCas = document.getElementById("max").value;
	//6.4.4 ends here
	
	var ustavi = false;
	
	var spremeniBarvo = function(id) {
		document.getElementById("stroboskop").style.backgroundColor = "#"+vrednosti[id];

		if (ustavi) {
			ustavi = false;
		} else {
			novId = (id+1) % vrednosti.length;
			timeout = Math.floor((Math.random() * (maxCas-minCas)) + minCas);
			setTimeout(function() {spremeniBarvo(novId)} , timeout);
		}		
	}
	//6.4.3
	var stop = function(event) {
		ustavi = true;
	
		
		
		var start = document.querySelector("#start");
		start.innerHTML = "Zaženi stroboskop";
		start.removeEventListener('click', stop);
		start.addEventListener('click', zagon);
		
	}	
	//6.4.3 ends here
	var zagon = function(event) {
		vrednosti = [];
		var barve = document.querySelectorAll("#barve > button");
		for (i = 0; i < barve.length; i++) {
			var barva = barve[i];
			vrednosti.push(barva.innerHTML);
		}
		
		//minCas = 1000;
		//maxCas = 1000;
		//6.4.4
		minCas = document.getElementById("min").value;;
		maxCas = document.getElementById("max").value;;
		//6.4.4 ends here
		spremeniBarvo(0);
		
		//6.4.3
		var start = document.querySelector("#start");
		start.innerHTML = "Ustavi stroboskop";
		start.removeEventListener('click', zagon);
		start.addEventListener('click', stop);
		//6.4.3 ends here
	}
	
	document.querySelector("#start").addEventListener('click', zagon);
		
	
	
});