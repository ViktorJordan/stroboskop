# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/ViktorJordan/stroboskop](https://bitbucket.org/ViktorJordan/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git commit -am "Priprava potrebnih JavaScript knjižnic"
git push origin master
```

Naloga 6.2.3:
https://bitbucket.org/ViktorJordan/stroboskop/commits/ba2f47063bc6e469954f79e7bcd9911d8d78fcbf

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/ViktorJordan/stroboskop/commits/c8172caf44b64e8e30c2903c2e52acfbb4045c90

Naloga 6.3.2:
https://bitbucket.org/ViktorJordan/stroboskop/commits/3df073a695e59ef9d2326eec1c8b6eaadedbb668?at=izgled

Naloga 6.3.3:
https://bitbucket.org/ViktorJordan/stroboskop/commits/0c4a76a38f6c5211d140d55fd005a35c817ca06c?at=izgled

Naloga 6.3.4:
https://bitbucket.org/ViktorJordan/stroboskop/commits/a02945de5c2df400113bfabaa186e7db717dd602?at=izgled

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/ViktorJordan/stroboskop/commits/b054808452e66166c2b6b73a152e41f26978155d?at=dinamika

Naloga 6.4.2:
https://bitbucket.org/ViktorJordan/stroboskop/commits/98f128fb292e336d17d615c14deb1bec5e6cc471?at=dinamika

Naloga 6.4.3:
https://bitbucket.org/ViktorJordan/stroboskop/commits/5a7a9840a1369e00580fbe591d8622eb36976f46?at=dinamika

Naloga 6.4.4:
https://bitbucket.org/ViktorJordan/stroboskop/commits/895bd2ace6a110b362d4b6a501bb97897ac93794?at=dinamika

Izdelal: Jordanoski ViktorJordan
vpisna : 63130340